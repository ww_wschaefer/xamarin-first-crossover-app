public enum RichTextEditorToolbarPresentationStyle : uint
{
	Modal,
	Popover
}

public enum ParagraphIndentation : uint
{
	Increase,
	Decrease
}

public enum RichTextEditorFeature : uint
{
	None = 0,
	Font = 1 << 0,
	FontSize = 1 << 1,
	Bold = 1 << 2,
	Italic = 1 << 3,
	Underline = 1 << 4,
	StrikeThrough = 1 << 5,
	TextAlignmentLeft = 1 << 6,
	TextAlignmentCenter = 1 << 7,
	TextAlignmentRight = 1 << 8,
	TextAlignmentJustified = 1 << 9,
	TextBackgroundColor = 1 << 10,
	TextForegroundColor = 1 << 11,
	ParagraphIndentation = 1 << 12,
	All = 1 << 13
}

public enum RichTextEditorColorPickerAction : uint
{
	ForegroudColor,
	BackgroundColor
}
