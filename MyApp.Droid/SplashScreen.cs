﻿using System;
using Android.App;
using Android.Graphics.Drawables;
using Cirrious.MvvmCross.Droid.Views;
using Cirrious.MvvmCross.Droid.Platform;

namespace MyApp.Droid
{
	[Activity (Label = "MyApp.Droid", MainLauncher = true, Icon = "@drawable/icon")]
	public class SplashScreen: MvxSplashScreenActivity
	{
		public SplashScreen () : base(Resource.Layout.SplashScreen)
		{
		}
	}
}

