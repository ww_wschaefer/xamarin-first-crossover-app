using System;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;

// @interface WEPopover
interface WEPopover
{
	// -(id)frameInView:(id)v;
	[Export ("frameInView:")]
	NSObject FrameInView (NSObject v);

	// -(id)superview;
	[Export ("superview")]
	[Verify (MethodToProperty)]
	NSObject Superview { get; }
}

// @interface WEPopoverAppDelegate : NSObject <UIApplicationDelegate>
[BaseType (typeof(NSObject))]
interface WEPopoverAppDelegate : IUIApplicationDelegate
{
	// @property (retain, nonatomic) UIWindow * window __attribute__((iboutlet));
	[Export ("window", ArgumentSemantic.Retain)]
	UIWindow Window { get; set; }

	// @property (retain, nonatomic) UINavigationController * navController __attribute__((iboutlet));
	[Export ("navController", ArgumentSemantic.Retain)]
	UINavigationController NavController { get; set; }
}

// @interface WEPopoverContainerViewProperties : NSObject
[BaseType (typeof(NSObject))]
interface WEPopoverContainerViewProperties
{
	// @property (retain, nonatomic) NSString * bgImageName;
	[Export ("bgImageName", ArgumentSemantic.Retain)]
	string BgImageName { get; set; }

	// @property (retain, nonatomic) NSString * upArrowImageName;
	[Export ("upArrowImageName", ArgumentSemantic.Retain)]
	string UpArrowImageName { get; set; }

	// @property (retain, nonatomic) NSString * downArrowImageName;
	[Export ("downArrowImageName", ArgumentSemantic.Retain)]
	string DownArrowImageName { get; set; }

	// @property (retain, nonatomic) NSString * leftArrowImageName;
	[Export ("leftArrowImageName", ArgumentSemantic.Retain)]
	string LeftArrowImageName { get; set; }

	// @property (retain, nonatomic) NSString * rightArrowImageName;
	[Export ("rightArrowImageName", ArgumentSemantic.Retain)]
	string RightArrowImageName { get; set; }

	// @property (assign, nonatomic) CGFloat leftBgMargin;
	[Export ("leftBgMargin")]
	nfloat LeftBgMargin { get; set; }

	// @property (assign, nonatomic) CGFloat rightBgMargin;
	[Export ("rightBgMargin")]
	nfloat RightBgMargin { get; set; }

	// @property (assign, nonatomic) CGFloat topBgMargin;
	[Export ("topBgMargin")]
	nfloat TopBgMargin { get; set; }

	// @property (assign, nonatomic) CGFloat bottomBgMargin;
	[Export ("bottomBgMargin")]
	nfloat BottomBgMargin { get; set; }

	// @property (assign, nonatomic) CGFloat leftContentMargin;
	[Export ("leftContentMargin")]
	nfloat LeftContentMargin { get; set; }

	// @property (assign, nonatomic) CGFloat rightContentMargin;
	[Export ("rightContentMargin")]
	nfloat RightContentMargin { get; set; }

	// @property (assign, nonatomic) CGFloat topContentMargin;
	[Export ("topContentMargin")]
	nfloat TopContentMargin { get; set; }

	// @property (assign, nonatomic) CGFloat bottomContentMargin;
	[Export ("bottomContentMargin")]
	nfloat BottomContentMargin { get; set; }

	// @property (assign, nonatomic) NSInteger topBgCapSize;
	[Export ("topBgCapSize")]
	nint TopBgCapSize { get; set; }

	// @property (assign, nonatomic) NSInteger leftBgCapSize;
	[Export ("leftBgCapSize")]
	nint LeftBgCapSize { get; set; }

	// @property (assign, nonatomic) CGFloat arrowMargin;
	[Export ("arrowMargin")]
	nfloat ArrowMargin { get; set; }
}

// @interface WEPopoverContainerView : UIView
[BaseType (typeof(UIView))]
interface WEPopoverContainerView
{
	// @property (readonly, nonatomic) UIPopoverArrowDirection arrowDirection;
	[Export ("arrowDirection")]
	UIPopoverArrowDirection ArrowDirection { get; }

	// @property (retain, nonatomic) UIView * contentView;
	[Export ("contentView", ArgumentSemantic.Retain)]
	UIView ContentView { get; set; }

	// -(id)initWithSize:(CGSize)theSize anchorRect:(CGRect)anchorRect displayArea:(CGRect)displayArea permittedArrowDirections:(UIPopoverArrowDirection)permittedArrowDirections properties:(WEPopoverContainerViewProperties *)properties;
	[Export ("initWithSize:anchorRect:displayArea:permittedArrowDirections:properties:")]
	IntPtr Constructor (CGSize theSize, CGRect anchorRect, CGRect displayArea, UIPopoverArrowDirection permittedArrowDirections, WEPopoverContainerViewProperties properties);

	// -(void)updatePositionWithAnchorRect:(CGRect)anchorRect displayArea:(CGRect)displayArea permittedArrowDirections:(UIPopoverArrowDirection)permittedArrowDirections;
	[Export ("updatePositionWithAnchorRect:displayArea:permittedArrowDirections:")]
	void UpdatePositionWithAnchorRect (CGRect anchorRect, CGRect displayArea, UIPopoverArrowDirection permittedArrowDirections);
}

// @interface WEPopoverContentViewController : UITableViewController
[BaseType (typeof(UITableViewController))]
interface WEPopoverContentViewController
{
}

// @protocol WETouchableViewDelegate <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface WETouchableViewDelegate
{
	// @required -(void)viewWasTouched:(WETouchableView *)view;
	[Abstract]
	[Export ("viewWasTouched:")]
	void ViewWasTouched (WETouchableView view);
}

// @interface WETouchableView : UIView
[BaseType (typeof(UIView))]
interface WETouchableView
{
	// @property (assign, nonatomic) BOOL touchForwardingDisabled;
	[Export ("touchForwardingDisabled")]
	bool TouchForwardingDisabled { get; set; }

	[Wrap ("WeakDelegate")]
	WETouchableViewDelegate Delegate { get; set; }

	// @property (assign, nonatomic) id<WETouchableViewDelegate> delegate;
	[NullAllowed, Export ("delegate", ArgumentSemantic.Assign)]
	NSObject WeakDelegate { get; set; }

	// @property (copy, nonatomic) NSArray * passthroughViews;
	[Export ("passthroughViews", ArgumentSemantic.Copy)]
	[Verify (StronglyTypedNSArray)]
	NSObject[] PassthroughViews { get; set; }
}

// @protocol WEPopoverControllerDelegate <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface WEPopoverControllerDelegate
{
	// @required -(void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController;
	[Abstract]
	[Export ("popoverControllerDidDismissPopover:")]
	void PopoverControllerDidDismissPopover (WEPopoverController popoverController);

	// @required -(BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController;
	[Abstract]
	[Export ("popoverControllerShouldDismissPopover:")]
	bool PopoverControllerShouldDismissPopover (WEPopoverController popoverController);
}

// @interface WEPopoverController : NSObject <WETouchableViewDelegate>
[BaseType (typeof(NSObject))]
interface WEPopoverController : IWETouchableViewDelegate
{
	// @property (retain, nonatomic) UIViewController * contentViewController;
	[Export ("contentViewController", ArgumentSemantic.Retain)]
	UIViewController ContentViewController { get; set; }

	// @property (readonly, nonatomic) UIView * view;
	[Export ("view")]
	UIView View { get; }

	// @property (readonly, getter = isPopoverVisible, nonatomic) BOOL popoverVisible;
	[Export ("popoverVisible")]
	bool PopoverVisible { [Bind ("isPopoverVisible")] get; }

	// @property (readonly, nonatomic) UIPopoverArrowDirection popoverArrowDirection;
	[Export ("popoverArrowDirection")]
	UIPopoverArrowDirection PopoverArrowDirection { get; }

	[Wrap ("WeakDelegate")]
	WEPopoverControllerDelegate Delegate { get; set; }

	// @property (assign, nonatomic) id<WEPopoverControllerDelegate> delegate;
	[NullAllowed, Export ("delegate", ArgumentSemantic.Assign)]
	NSObject WeakDelegate { get; set; }

	// @property (assign, nonatomic) CGSize popoverContentSize;
	[Export ("popoverContentSize", ArgumentSemantic.Assign)]
	CGSize PopoverContentSize { get; set; }

	// @property (retain, nonatomic) WEPopoverContainerViewProperties * containerViewProperties;
	[Export ("containerViewProperties", ArgumentSemantic.Retain)]
	WEPopoverContainerViewProperties ContainerViewProperties { get; set; }

	// @property (retain, nonatomic) id<NSObject> context;
	[Export ("context", ArgumentSemantic.Retain)]
	NSObject Context { get; set; }

	// @property (copy, nonatomic) NSArray * passthroughViews;
	[Export ("passthroughViews", ArgumentSemantic.Copy)]
	[Verify (StronglyTypedNSArray)]
	NSObject[] PassthroughViews { get; set; }

	// -(id)initWithContentViewController:(UIViewController *)theContentViewController;
	[Export ("initWithContentViewController:")]
	IntPtr Constructor (UIViewController theContentViewController);

	// -(void)dismissPopoverAnimated:(BOOL)animated;
	[Export ("dismissPopoverAnimated:")]
	void DismissPopoverAnimated (bool animated);

	// -(void)presentPopoverFromBarButtonItem:(UIBarButtonItem *)item permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated;
	[Export ("presentPopoverFromBarButtonItem:permittedArrowDirections:animated:")]
	void PresentPopoverFromBarButtonItem (UIBarButtonItem item, UIPopoverArrowDirection arrowDirections, bool animated);

	// -(void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated;
	[Export ("presentPopoverFromRect:inView:permittedArrowDirections:animated:")]
	void PresentPopoverFromRect (CGRect rect, UIView view, UIPopoverArrowDirection arrowDirections, bool animated);

	// -(void)repositionPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections;
	[Export ("repositionPopoverFromRect:inView:permittedArrowDirections:")]
	void RepositionPopoverFromRect (CGRect rect, UIView view, UIPopoverArrowDirection arrowDirections);
}

// @protocol WEPopoverParentView
[Protocol, Model]
interface WEPopoverParentView
{
	// @optional -(CGRect)displayAreaForPopover;
	[Export ("displayAreaForPopover")]
	[Verify (MethodToProperty)]
	CGRect DisplayAreaForPopover { get; }
}

// @interface WEPopoverTableViewController : UITableViewController <WEPopoverControllerDelegate, UIPopoverControllerDelegate>
[BaseType (typeof(UITableViewController))]
interface WEPopoverTableViewController : IWEPopoverControllerDelegate, IUIPopoverControllerDelegate
{
	// @property (retain, nonatomic) WEPopoverController * popoverController;
	[Export ("popoverController", ArgumentSemantic.Retain)]
	WEPopoverController PopoverController { get; set; }

	// -(void)showPopover:(id)sender __attribute__((ibaction));
	[Export ("showPopover:")]
	void ShowPopover (NSObject sender);
}

// @interface WEPopoverViewController : UIViewController
[BaseType (typeof(UIViewController))]
interface WEPopoverViewController
{
	// @property (retain, nonatomic) WEPopoverController * popoverController;
	[Export ("popoverController", ArgumentSemantic.Retain)]
	WEPopoverController PopoverController { get; set; }

	// -(void)onButtonClick:(UIButton *)button __attribute__((ibaction));
	[Export ("onButtonClick:")]
	void OnButtonClick (UIButton button);
}

// @interface RichTextEditor (UIFont)
[Category]
[BaseType (typeof(UIFont))]
interface UIFont_RichTextEditor
{
	// +(NSString *)postscriptNameFromFullName:(NSString *)fullName;
	[Static]
	[Export ("postscriptNameFromFullName:")]
	string PostscriptNameFromFullName (string fullName);

	// +(UIFont *)fontWithName:(NSString *)name size:(CGFloat)size boldTrait:(BOOL)isBold italicTrait:(BOOL)isItalic;
	[Static]
	[Export ("fontWithName:size:boldTrait:italicTrait:")]
	UIFont FontWithName (string name, nfloat size, bool isBold, bool isItalic);

	// -(UIFont *)fontWithBoldTrait:(BOOL)bold italicTrait:(BOOL)italic andSize:(CGFloat)size;
	[Export ("fontWithBoldTrait:italicTrait:andSize:")]
	UIFont FontWithBoldTrait (bool bold, bool italic, nfloat size);

	// -(UIFont *)fontWithBoldTrait:(BOOL)bold andItalicTrait:(BOOL)italic;
	[Export ("fontWithBoldTrait:andItalicTrait:")]
	UIFont FontWithBoldTrait (bool bold, bool italic);

	// -(BOOL)isBold;
	[Export ("isBold")]
	[Verify (MethodToProperty)]
	bool IsBold { get; }

	// -(BOOL)isItalic;
	[Export ("isItalic")]
	[Verify (MethodToProperty)]
	bool IsItalic { get; }
}

// @interface RichTextEditor (NSAttributedString)
[Category]
[BaseType (typeof(NSAttributedString))]
interface NSAttributedString_RichTextEditor
{
	// -(NSRange)firstParagraphRangeFromTextRange:(NSRange)range;
	[Export ("firstParagraphRangeFromTextRange:")]
	NSRange FirstParagraphRangeFromTextRange (NSRange range);

	// -(NSArray *)rangeOfParagraphsFromTextRange:(NSRange)textRange;
	[Export ("rangeOfParagraphsFromTextRange:")]
	[Verify (StronglyTypedNSArray)]
	NSObject[] RangeOfParagraphsFromTextRange (NSRange textRange);

	// -(NSString *)htmlString;
	[Export ("htmlString")]
	[Verify (MethodToProperty)]
	string HtmlString { get; }
}

// @protocol RichTextEditorToolbarDelegate <UIScrollViewDelegate>
[Protocol, Model]
interface RichTextEditorToolbarDelegate : IUIScrollViewDelegate
{
	// @required -(void)richTextEditorToolbarDidSelectBold;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectBold")]
	void RichTextEditorToolbarDidSelectBold ();

	// @required -(void)richTextEditorToolbarDidSelectItalic;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectItalic")]
	void RichTextEditorToolbarDidSelectItalic ();

	// @required -(void)richTextEditorToolbarDidSelectUnderline;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectUnderline")]
	void RichTextEditorToolbarDidSelectUnderline ();

	// @required -(void)richTextEditorToolbarDidSelectStrikeThrough;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectStrikeThrough")]
	void RichTextEditorToolbarDidSelectStrikeThrough ();

	// @required -(void)richTextEditorToolbarDidSelectBulletPoint;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectBulletPoint")]
	void RichTextEditorToolbarDidSelectBulletPoint ();

	// @required -(void)richTextEditorToolbarDidSelectParagraphIndentation:(ParagraphIndentation)paragraphIndentation;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectParagraphIndentation:")]
	void RichTextEditorToolbarDidSelectParagraphIndentation (ParagraphIndentation paragraphIndentation);

	// @required -(void)richTextEditorToolbarDidSelectFontSize:(NSNumber *)fontSize;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectFontSize:")]
	void RichTextEditorToolbarDidSelectFontSize (NSNumber fontSize);

	// @required -(void)richTextEditorToolbarDidSelectFontWithName:(NSString *)fontName;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectFontWithName:")]
	void RichTextEditorToolbarDidSelectFontWithName (string fontName);

	// @required -(void)richTextEditorToolbarDidSelectTextBackgroundColor:(UIColor *)color;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectTextBackgroundColor:")]
	void RichTextEditorToolbarDidSelectTextBackgroundColor (UIColor color);

	// @required -(void)richTextEditorToolbarDidSelectTextForegroundColor:(UIColor *)color;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectTextForegroundColor:")]
	void RichTextEditorToolbarDidSelectTextForegroundColor (UIColor color);

	// @required -(void)richTextEditorToolbarDidSelectTextAlignment:(NSTextAlignment)textAlignment;
	[Abstract]
	[Export ("richTextEditorToolbarDidSelectTextAlignment:")]
	void RichTextEditorToolbarDidSelectTextAlignment (NSTextAlignment textAlignment);
}

// @protocol RichTextEditorToolbarDataSource <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface RichTextEditorToolbarDataSource
{
	// @required -(NSArray *)fontSizeSelectionForRichTextEditorToolbar;
	[Abstract]
	[Export ("fontSizeSelectionForRichTextEditorToolbar")]
	[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
	NSObject[] FontSizeSelectionForRichTextEditorToolbar { get; }

	// @required -(NSArray *)fontFamilySelectionForRichTextEditorToolbar;
	[Abstract]
	[Export ("fontFamilySelectionForRichTextEditorToolbar")]
	[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
	NSObject[] FontFamilySelectionForRichTextEditorToolbar { get; }

	// @required -(RichTextEditorToolbarPresentationStyle)presentarionStyleForRichTextEditorToolbar;
	[Abstract]
	[Export ("presentarionStyleForRichTextEditorToolbar")]
	[Verify (MethodToProperty)]
	RichTextEditorToolbarPresentationStyle PresentarionStyleForRichTextEditorToolbar { get; }

	// @required -(UIModalPresentationStyle)modalPresentationStyleForRichTextEditorToolbar;
	[Abstract]
	[Export ("modalPresentationStyleForRichTextEditorToolbar")]
	[Verify (MethodToProperty)]
	UIModalPresentationStyle ModalPresentationStyleForRichTextEditorToolbar { get; }

	// @required -(UIModalTransitionStyle)modalTransitionStyleForRichTextEditorToolbar;
	[Abstract]
	[Export ("modalTransitionStyleForRichTextEditorToolbar")]
	[Verify (MethodToProperty)]
	UIModalTransitionStyle ModalTransitionStyleForRichTextEditorToolbar { get; }

	// @required -(UIViewController *)firsAvailableViewControllerForRichTextEditorToolbar;
	[Abstract]
	[Export ("firsAvailableViewControllerForRichTextEditorToolbar")]
	[Verify (MethodToProperty)]
	UIViewController FirsAvailableViewControllerForRichTextEditorToolbar { get; }

	// @required -(RichTextEditorFeature)featuresEnabledForRichTextEditorToolbar;
	[Abstract]
	[Export ("featuresEnabledForRichTextEditorToolbar")]
	[Verify (MethodToProperty)]
	RichTextEditorFeature FeaturesEnabledForRichTextEditorToolbar { get; }
}

// @interface RichTextEditorToolbar : UIScrollView
[BaseType (typeof(UIScrollView))]
interface RichTextEditorToolbar
{
	[Wrap ("WeakDelegate")]
	[NullAllowed]
	RichTextEditorToolbarDelegate Delegate { get; set; }

	// @property (nonatomic, weak) id<RichTextEditorToolbarDelegate> _Nullable delegate;
	[NullAllowed, Export ("delegate", ArgumentSemantic.Weak)]
	NSObject WeakDelegate { get; set; }

	// @property (nonatomic, weak) id<RichTextEditorToolbarDataSource> _Nullable dataSource;
	[NullAllowed, Export ("dataSource", ArgumentSemantic.Weak)]
	RichTextEditorToolbarDataSource DataSource { get; set; }

	// -(id)initWithFrame:(CGRect)frame delegate:(id<RichTextEditorToolbarDelegate>)delegate dataSource:(id<RichTextEditorToolbarDataSource>)dataSource;
	[Export ("initWithFrame:delegate:dataSource:")]
	IntPtr Constructor (CGRect frame, RichTextEditorToolbarDelegate @delegate, RichTextEditorToolbarDataSource dataSource);

	// -(void)updateStateWithAttributes:(NSDictionary *)attributes;
	[Export ("updateStateWithAttributes:")]
	void UpdateStateWithAttributes (NSDictionary attributes);
}

// @protocol RichTextEditorDataSource <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface RichTextEditorDataSource
{
	// @optional -(NSArray *)fontSizeSelectionForRichTextEditor:(RichTextEditor *)richTextEditor;
	[Export ("fontSizeSelectionForRichTextEditor:")]
	[Verify (StronglyTypedNSArray)]
	NSObject[] FontSizeSelectionForRichTextEditor (RichTextEditor richTextEditor);

	// @optional -(NSArray *)fontFamilySelectionForRichTextEditor:(RichTextEditor *)richTextEditor;
	[Export ("fontFamilySelectionForRichTextEditor:")]
	[Verify (StronglyTypedNSArray)]
	NSObject[] FontFamilySelectionForRichTextEditor (RichTextEditor richTextEditor);

	// @optional -(RichTextEditorToolbarPresentationStyle)presentarionStyleForRichTextEditor:(RichTextEditor *)richTextEditor;
	[Export ("presentarionStyleForRichTextEditor:")]
	RichTextEditorToolbarPresentationStyle PresentarionStyleForRichTextEditor (RichTextEditor richTextEditor);

	// @optional -(UIModalPresentationStyle)modalPresentationStyleForRichTextEditor:(RichTextEditor *)richTextEditor;
	[Export ("modalPresentationStyleForRichTextEditor:")]
	UIModalPresentationStyle ModalPresentationStyleForRichTextEditor (RichTextEditor richTextEditor);

	// @optional -(UIModalTransitionStyle)modalTransitionStyleForRichTextEditor:(RichTextEditor *)richTextEditor;
	[Export ("modalTransitionStyleForRichTextEditor:")]
	UIModalTransitionStyle ModalTransitionStyleForRichTextEditor (RichTextEditor richTextEditor);

	// @optional -(RichTextEditorFeature)featuresEnabledForRichTextEditor:(RichTextEditor *)richTextEditor;
	[Export ("featuresEnabledForRichTextEditor:")]
	RichTextEditorFeature FeaturesEnabledForRichTextEditor (RichTextEditor richTextEditor);

	// @optional -(BOOL)shouldDisplayToolbarForRichTextEditor:(RichTextEditor *)richTextEditor;
	[Export ("shouldDisplayToolbarForRichTextEditor:")]
	bool ShouldDisplayToolbarForRichTextEditor (RichTextEditor richTextEditor);

	// @optional -(BOOL)shouldDisplayRichTextOptionsInMenuControllerForRichTextrEditor:(RichTextEditor *)richTextEdiotor;
	[Export ("shouldDisplayRichTextOptionsInMenuControllerForRichTextrEditor:")]
	bool ShouldDisplayRichTextOptionsInMenuControllerForRichTextrEditor (RichTextEditor richTextEdiotor);
}

// @interface RichTextEditor : UITextView
[BaseType (typeof(UITextView))]
interface RichTextEditor
{
	// @property (nonatomic, weak) id<RichTextEditorDataSource> _Nullable dataSource __attribute__((iboutlet));
	[NullAllowed, Export ("dataSource", ArgumentSemantic.Weak)]
	RichTextEditorDataSource DataSource { get; set; }

	// @property (assign, nonatomic) CGFloat defaultIndentationSize;
	[Export ("defaultIndentationSize")]
	nfloat DefaultIndentationSize { get; set; }

	// -(NSString *)htmlString;
	[Export ("htmlString")]
	[Verify (MethodToProperty)]
	string HtmlString { get; }
}

// @interface RichTextEditor (UIView)
[Category]
[BaseType (typeof(UIView))]
interface UIView_RichTextEditor
{
	// -(UIColor *)colorOfPoint:(CGPoint)point;
	[Export ("colorOfPoint:")]
	UIColor ColorOfPoint (CGPoint point);

	// -(UIViewController *)firstAvailableViewController;
	[Export ("firstAvailableViewController")]
	[Verify (MethodToProperty)]
	UIViewController FirstAvailableViewController { get; }
}

// @protocol RichTextEditorColorPickerViewControllerDelegate <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface RichTextEditorColorPickerViewControllerDelegate
{
	// @required -(void)richTextEditorColorPickerViewControllerDidSelectColor:(UIColor *)color withAction:(RichTextEditorColorPickerAction)action;
	[Abstract]
	[Export ("richTextEditorColorPickerViewControllerDidSelectColor:withAction:")]
	void RichTextEditorColorPickerViewControllerDidSelectColor (UIColor color, RichTextEditorColorPickerAction action);

	// @required -(void)richTextEditorColorPickerViewControllerDidSelectClose;
	[Abstract]
	[Export ("richTextEditorColorPickerViewControllerDidSelectClose")]
	void RichTextEditorColorPickerViewControllerDidSelectClose ();
}

// @protocol RichTextEditorColorPickerViewControllerDataSource <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface RichTextEditorColorPickerViewControllerDataSource
{
	// @required -(BOOL)richTextEditorColorPickerViewControllerShouldDisplayToolbar;
	[Abstract]
	[Export ("richTextEditorColorPickerViewControllerShouldDisplayToolbar")]
	[Verify (MethodToProperty)]
	bool RichTextEditorColorPickerViewControllerShouldDisplayToolbar { get; }
}

// @interface RichTextEditorColorPickerViewController : UIViewController
[BaseType (typeof(UIViewController))]
interface RichTextEditorColorPickerViewController
{
	[Wrap ("WeakDelegate")]
	[NullAllowed]
	RichTextEditorColorPickerViewControllerDelegate Delegate { get; set; }

	// @property (nonatomic, weak) id<RichTextEditorColorPickerViewControllerDelegate> _Nullable delegate;
	[NullAllowed, Export ("delegate", ArgumentSemantic.Weak)]
	NSObject WeakDelegate { get; set; }

	// @property (nonatomic, weak) id<RichTextEditorColorPickerViewControllerDataSource> _Nullable dataSource;
	[NullAllowed, Export ("dataSource", ArgumentSemantic.Weak)]
	RichTextEditorColorPickerViewControllerDataSource DataSource { get; set; }

	// @property (assign, nonatomic) RichTextEditorColorPickerAction action;
	[Export ("action", ArgumentSemantic.Assign)]
	RichTextEditorColorPickerAction Action { get; set; }

	// @property (nonatomic, strong) UIImageView * colorsImageView;
	[Export ("colorsImageView", ArgumentSemantic.Strong)]
	UIImageView ColorsImageView { get; set; }

	// @property (nonatomic, strong) UIView * selectedColorView;
	[Export ("selectedColorView", ArgumentSemantic.Strong)]
	UIView SelectedColorView { get; set; }

	// -(void)doneSelected:(id)sender __attribute__((ibaction));
	[Export ("doneSelected:")]
	void DoneSelected (NSObject sender);

	// -(void)closeSelected:(id)sender __attribute__((ibaction));
	[Export ("closeSelected:")]
	void CloseSelected (NSObject sender);
}

// @protocol RichTextEditorFontPickerViewControllerDelegate <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface RichTextEditorFontPickerViewControllerDelegate
{
	// @required -(void)richTextEditorFontPickerViewControllerDidSelectFontWithName:(NSString *)fontName;
	[Abstract]
	[Export ("richTextEditorFontPickerViewControllerDidSelectFontWithName:")]
	void RichTextEditorFontPickerViewControllerDidSelectFontWithName (string fontName);

	// @required -(void)richTextEditorFontPickerViewControllerDidSelectClose;
	[Abstract]
	[Export ("richTextEditorFontPickerViewControllerDidSelectClose")]
	void RichTextEditorFontPickerViewControllerDidSelectClose ();
}

// @protocol RichTextEditorFontPickerViewControllerDataSource <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface RichTextEditorFontPickerViewControllerDataSource
{
	// @required -(NSArray *)richTextEditorFontPickerViewControllerCustomFontFamilyNamesForSelection;
	[Abstract]
	[Export ("richTextEditorFontPickerViewControllerCustomFontFamilyNamesForSelection")]
	[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
	NSObject[] RichTextEditorFontPickerViewControllerCustomFontFamilyNamesForSelection { get; }

	// @required -(BOOL)richTextEditorFontPickerViewControllerShouldDisplayToolbar;
	[Abstract]
	[Export ("richTextEditorFontPickerViewControllerShouldDisplayToolbar")]
	[Verify (MethodToProperty)]
	bool RichTextEditorFontPickerViewControllerShouldDisplayToolbar { get; }
}

// @interface RichTextEditorFontPickerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
[BaseType (typeof(UIViewController))]
interface RichTextEditorFontPickerViewController : IUITableViewDelegate, IUITableViewDataSource
{
	[Wrap ("WeakDelegate")]
	[NullAllowed]
	RichTextEditorFontPickerViewControllerDelegate Delegate { get; set; }

	// @property (nonatomic, weak) id<RichTextEditorFontPickerViewControllerDelegate> _Nullable delegate;
	[NullAllowed, Export ("delegate", ArgumentSemantic.Weak)]
	NSObject WeakDelegate { get; set; }

	// @property (nonatomic, weak) id<RichTextEditorFontPickerViewControllerDataSource> _Nullable dataSource;
	[NullAllowed, Export ("dataSource", ArgumentSemantic.Weak)]
	RichTextEditorFontPickerViewControllerDataSource DataSource { get; set; }

	// @property (nonatomic, strong) UITableView * tableview;
	[Export ("tableview", ArgumentSemantic.Strong)]
	UITableView Tableview { get; set; }

	// @property (nonatomic, strong) NSArray * fontNames;
	[Export ("fontNames", ArgumentSemantic.Strong)]
	[Verify (StronglyTypedNSArray)]
	NSObject[] FontNames { get; set; }
}

// @protocol RichTextEditorFontSizePickerViewControllerDelegate <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface RichTextEditorFontSizePickerViewControllerDelegate
{
	// @required -(void)richTextEditorFontSizePickerViewControllerDidSelectFontSize:(NSNumber *)fontSize;
	[Abstract]
	[Export ("richTextEditorFontSizePickerViewControllerDidSelectFontSize:")]
	void RichTextEditorFontSizePickerViewControllerDidSelectFontSize (NSNumber fontSize);

	// @required -(void)richTextEditorFontSizePickerViewControllerDidSelectClose;
	[Abstract]
	[Export ("richTextEditorFontSizePickerViewControllerDidSelectClose")]
	void RichTextEditorFontSizePickerViewControllerDidSelectClose ();
}

// @protocol RichTextEditorFontSizePickerViewControllerDataSource <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface RichTextEditorFontSizePickerViewControllerDataSource
{
	// @required -(BOOL)richTextEditorFontSizePickerViewControllerShouldDisplayToolbar;
	[Abstract]
	[Export ("richTextEditorFontSizePickerViewControllerShouldDisplayToolbar")]
	[Verify (MethodToProperty)]
	bool RichTextEditorFontSizePickerViewControllerShouldDisplayToolbar { get; }

	// @required -(NSArray *)richTextEditorFontSizePickerViewControllerCustomFontSizesForSelection;
	[Abstract]
	[Export ("richTextEditorFontSizePickerViewControllerCustomFontSizesForSelection")]
	[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
	NSObject[] RichTextEditorFontSizePickerViewControllerCustomFontSizesForSelection { get; }
}

// @interface RichTextEditorFontSizePickerViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
[BaseType (typeof(UIViewController))]
interface RichTextEditorFontSizePickerViewController : IUITableViewDataSource, IUITableViewDelegate
{
	[Wrap ("WeakDelegate")]
	[NullAllowed]
	RichTextEditorFontSizePickerViewControllerDelegate Delegate { get; set; }

	// @property (nonatomic, weak) id<RichTextEditorFontSizePickerViewControllerDelegate> _Nullable delegate;
	[NullAllowed, Export ("delegate", ArgumentSemantic.Weak)]
	NSObject WeakDelegate { get; set; }

	// @property (nonatomic, weak) id<RichTextEditorFontSizePickerViewControllerDataSource> _Nullable dataSource;
	[NullAllowed, Export ("dataSource", ArgumentSemantic.Weak)]
	RichTextEditorFontSizePickerViewControllerDataSource DataSource { get; set; }

	// @property (nonatomic, strong) UITableView * tableview;
	[Export ("tableview", ArgumentSemantic.Strong)]
	UITableView Tableview { get; set; }

	// @property (nonatomic, strong) NSArray * fontSizes;
	[Export ("fontSizes", ArgumentSemantic.Strong)]
	[Verify (StronglyTypedNSArray)]
	NSObject[] FontSizes { get; set; }
}

// @protocol RichTextEditorPopover <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface RichTextEditorPopover
{
	// @required -(id)initWithContentViewController:(UIViewController *)viewController;
	[Abstract]
	[Export ("initWithContentViewController:")]
	IntPtr Constructor (UIViewController viewController);

	// @required -(void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated;
	[Abstract]
	[Export ("presentPopoverFromRect:inView:permittedArrowDirections:animated:")]
	void PresentPopoverFromRect (CGRect rect, UIView view, UIPopoverArrowDirection arrowDirections, bool animated);

	// @required -(void)presentPopoverFromBarButtonItem:(UIBarButtonItem *)item permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated;
	[Abstract]
	[Export ("presentPopoverFromBarButtonItem:permittedArrowDirections:animated:")]
	void PresentPopoverFromBarButtonItem (UIBarButtonItem item, UIPopoverArrowDirection arrowDirections, bool animated);

	// @required -(void)dismissPopoverAnimated:(BOOL)animated;
	[Abstract]
	[Export ("dismissPopoverAnimated:")]
	void DismissPopoverAnimated (bool animated);
}

// @interface RichTextEditorToggleButton : UIButton
[BaseType (typeof(UIButton))]
interface RichTextEditorToggleButton
{
	// @property (assign, nonatomic) BOOL on;
	[Export ("on")]
	bool On { get; set; }
}
