﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite.Net;
using SQLite.Net.Attributes;
using SQLite.Net.Interop;
using Path = System.IO.Path;
using JeffWilcox.Utilities.Silverlight;

namespace MyApp.DB
{
	public class DBCommonItem
	{
		// ID
		[PrimaryKey]
		public string Id { get; set; }

		// Title
		private string _title;
		public string Title { 
			get { 
				return _title;
			} 
			set {
				_title = value;
				var test = MD5.GetMd5String (_title);
				Id = test;
			}
		}

		// Link
		public string Link { get; set; }

		// Description
		public string Description { get; set; }

		// PubDate
		private string _pubDate;
		public string PubDate { 
			get {
				return _pubDate;
			} 
			set { 
				_pubDate = value;
				try
				{
					DateTime dateTime = DateTime.Parse (_pubDate);
					Date = dateTime;
				}
				catch (Exception e)
				{
					e.ToString();
				}
			}
		}
		public DateTime Date { get; set; }

		// htmlContent
		public string htmlContent { get; set; }

		// Constructur
		public DBCommonItem ()
		{
		}

		// Methods
		public override string ToString ()
		{
			return string.Format ("[DBCommonItem: Id={0} Title={1} PubDate={2}]", Id, Title, PubDate);
		}
	}
}

