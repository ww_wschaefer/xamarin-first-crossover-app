﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite.Net;
using SQLite.Net.Attributes;
using SQLite.Net.Interop;
using Path = System.IO.Path;
using SQLite.Net.Platform.XamarinIOS;

namespace MyApp.DB
{
	public class Database : SQLiteConnection {

		public Database (string path) : base(new SQLitePlatformIOS(), path)
		{
			CreateTable<DBCommonItem> ();
			CreateTable<DBItem> ();

			//var all = QueryAllStocks ().ToList();
		}

		// select

		public IEnumerable<DBItem> QueryAllItems ()
		{
//			return	from i in Table<DBItem> () orderby i.Date select i;

			//(from i in Table<T> () select i).ToList ();
			return (Table<DBItem>().OrderByDescending(i => i.Date).Select(i => i));
		}

		public DBItem QueryItem (string id)
		{
			var query = Table<DBItem>().Where(v => v.Id.Equals(id));
			return query.FirstOrDefault ();
		}
	}
}

