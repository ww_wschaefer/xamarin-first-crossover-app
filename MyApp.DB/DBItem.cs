﻿using System;

namespace MyApp.DB
{
	public class DBItem : DBCommonItem
	{
		public string Guid { get; set; }
		public string MediaThumbnail { get; set; }
		public string MediaContent { get; set; }

		public DBItem ()
		{
		}
	}
}

