﻿using System;

namespace MyApp.Core
{
	public class RssImage
	{
		private string _title;
		private string _url;
		private string _link;
		private string _description;

		public string Title
		{
			get {return _title;}
			set {_title = value;}
		}

		public string Url
		{
			get {return _url;}
			set {_url = value;}
		}

		public string Link
		{
			get {return _link;}
			set {_link = value;}
		}

		public string Description
		{
			get {return _description;}
			set {_description = value;}
		}

		public RssImage ()
		{
		}
	}
}

