﻿using System;

namespace MyApp.Core
{
	public class RssItem : RssCommonItem
	{
		private string _guid;
		private string _mediaThumbnail;
		private string _mediaContent;

		public string Guid
		{
			get {return _guid;}
			set {_guid = value;}
		}

		public string MediaThumbnail
		{
			get {return _mediaThumbnail;}
			set {_mediaThumbnail = value;}
		}

		public string MediaContent
		{
			get {return _mediaContent;}
			set {_mediaContent = value;}
		}

		public RssItem ()
		{
		}
	}
}

