﻿using System;
using MyApp.DB;

namespace MyApp.Core
{
	public class RssChannel : RssCommonItem
	{
		private string _language;
		private string _copyright;

		private string _lastBuildDate;
		private string _ttl;
		private RssImage _image;
		private DBItem[] _items;

		public string Language
		{
			get {return _language;}
			set {_language = value;}
		}

		public string Copyright
		{
			get {return _copyright;}
			set {_copyright = value;}
		}

		public string LastBuildDate
		{
			get {return _lastBuildDate;}
			set {_lastBuildDate = value;}
		}

		public string Ttl
		{
			get {return _ttl;}
			set {_ttl = value;}
		}

		public RssImage Image
		{
			get {return _image;}
			set {_image = value;}
		}

		public DBItem[] Items
		{
			get {return _items;}
			set {_items = value;}
		}

		public RssChannel ()
		{
			
		}
	}
}

