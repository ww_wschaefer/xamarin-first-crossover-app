﻿using System;

namespace MyApp.Core
{
	public class RssCommonItem
	{
		private string _title;
		private string _link;
		private string _description;
		private string _pubDate;

		public string Title
		{
			get {return _title;}
			set {_title = value;}
		}

		public string Link
		{
			get {return _link;}
			set {_link = value;}
		}

		public string Description
		{
			get {return _description;}
			set {_description = value;}
		}

		public string PubDate
		{
			get {return _pubDate;}
			set {_pubDate = value;}
		}

		public RssCommonItem ()
		{
		}
	}
}

