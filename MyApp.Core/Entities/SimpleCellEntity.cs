﻿using System;

namespace MyApp.Core
{
	public class SimpleCellEntity
	{
		private string _title;
		private string _description;
		private string _iconUrl;

		public string Title {
			get 
			{
				return _title;
			}
			 set 
			{
				_title = value;	
			}
		}

		public string Description {
			get 
			{
				return _description;
			}
			set 
			{
				_description = value;	
			}
		}

		public string IconUrl {
			get 
			{
				return _iconUrl;
			}
			set 
			{
				_iconUrl = value;	
			}
		}

		public SimpleCellEntity ()
		{
		}
	}
}

