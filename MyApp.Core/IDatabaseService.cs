﻿using System;

namespace MyApp.Core
{
	public interface IDatabaseService
	{
		string GetDatabasePath(string dbName);
	}
}

