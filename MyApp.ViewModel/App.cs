﻿using System;
using MvvmCross.Platform;
using MyApp.ViewModel;
using MvvmCross.Core.ViewModels;

namespace MyApp.Core
{
	public class App : MvxApplication
	{
		public App ()
		{
		}
		public override void Initialize ()
		{
			RegisterAppStart<FirstViewModel> ();
		}
	}
}

