﻿using System;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core;
using Chance.MvvmCross.Plugins.UserInteraction;
using System.Net;
using System.IO;
using System.Text;
using System.Windows.Input;
using System.Xml;
using System.Collections.Generic;
using MyApp.Core;
using MyApp.ViewModel;
using MyApp.Manager;
using MyApp.DB;

public enum RequestState
{
	None,
	Started,
	Finished,
	Failed
};

namespace MyApp.ViewModel
{
	public class FirstViewModel : MvxViewModel
	{
		// rss url
		private string _rssUrl;
		// request state
		private RequestState _status = RequestState.None;
		public RequestState RequestStatus
		{
			get { return _status; }
			set {
				_status = value;
				RaisePropertyChanged (() => RequestStatus);
			}
		}
		private RssChannel[] _rssChannels;
		public RssChannel[] RssChannels
		{
			get { return _rssChannels; }
			set { 
				_rssChannels = value;
				RaisePropertyChanged (() => RssChannels);
				if (_rssChannels.Length >= 0) {
					DBItem[] items = _rssChannels [0].Items;

					List<DBItem> downloadedItems = new List<DBItem> (items);
					SaveItemsToDb (downloadedItems);
					GetItemsFromDb (false);
				}
			}
		}

		// db items
		private DBItem[] _dbItems;
		public DBItem[] DBItems
		{
			get { return _dbItems; }
			set {
				_dbItems = value;
				RaisePropertyChanged (() => DBItems);
			}
		}

		// actions
		public Action<string>OnDownloadSucceeded;
		public Action<string>OnDownloadFailed;

		// 	feed request
		public void feedRequest() {
			if (_rssUrl != null && _rssUrl.Length > 0) {
				RequestManager.Instance.requestWithUrl (_rssUrl, RequestSuccessCallback, RequestErrorCallback);
			} else {
				#warning TODO
				// show alert
			}
		}

		public void RequestSuccessCallback(string responseString)
		{
			RequestStatus = RequestState.Finished;
			parseXml (responseString);
			System.Diagnostics.Debug.WriteLine (responseString);
			if (OnDownloadSucceeded != null) {
				OnDownloadSucceeded (responseString);
			}
		}
		public void RequestErrorCallback(Exception e)
		{
			RequestStatus = RequestState.Failed;
			System.Diagnostics.Debug.WriteLine (e.ToString());
			if (OnDownloadFailed != null) {
				OnDownloadFailed (e.ToString ());
			}
		}

		public void parseXml(string xmlString) {
			try {
				XmlReader xmlReader = XmlReader.Create (new System.IO.StringReader(xmlString));

				List<RssChannel> channelList = new List<RssChannel>();
				RssChannel rssChannel = null;
				List<DBItem> itemList = null;
				DBItem rssItem = null;

				string titleString = null;
				string descriptionString = null;
				string linkString = null;
				string pubDateString = null;

				string nodeName = null;

				while (xmlReader.Read ()) {
					switch (xmlReader.NodeType)
					{
					case XmlNodeType.Element: {
							nodeName = xmlReader.Name;
						switch (xmlReader.Name) {
							case "channel":
								// create new channel
								rssChannel = new RssChannel();
								itemList = new List<DBItem>();
								break;
							case "item":
								// crate item
								rssItem = new DBItem();
								break;
							case "image":
								break;
							case "pubDate":
								//
								break;
							case "media:thumbnail":
								// get attribute
								string url = xmlReader.GetAttribute("url");
								rssItem.MediaThumbnail = url;
								break;
						}
						break;
					}
					case XmlNodeType.Text:
						{
							switch (nodeName) {
							case "title":
								titleString = xmlReader.Value;
								break;
							case "description":
								descriptionString = xmlReader.Value;
								break;
							case "link":
								linkString = xmlReader.Value;
								break;
							case "pubDate":
								pubDateString = xmlReader.Value;
								break;
							}
						}
						break;
					case XmlNodeType.EndElement:{
							switch (xmlReader.Name) {
							case "channel":
								rssChannel.Items = itemList.ToArray();
								rssChannel.Title = titleString;
								rssChannel.Description = descriptionString;
								rssChannel.Link = linkString;
								rssChannel.PubDate = pubDateString;
								// add to list
								channelList.Add(rssChannel);
								break;
							case "item":
								rssItem.Title = titleString;
								rssItem.Description = descriptionString;
								rssItem.Link = linkString;
								rssItem.PubDate = pubDateString;
								// add to list
								itemList.Add(rssItem);
								break;
							case "image":
								break;
							}
						}
						break;
					}
				}
				RssChannels = channelList.ToArray();

			} catch (Exception ex) {
				System.Diagnostics.Debug.WriteLine ("error: " + ex.ToString());
			}
		}

		public void someAction() {
			System.Diagnostics.Debug.WriteLine ("some action is called in view model");
		}
		public ICommand SomeActionCommand
		{
			get
			{
				return new MvxCommand(feedRequest);
			}
		}
		public ICommand ItemSelectedCommand
		{
			get 
			{ 
				return new MvxCommand (someAction);
			}
		}

		public void DidSelectRow(int row)
		{
			DBItem selectedItem = DBItems [row];
			ShowViewModel<RssDetailViewModel>(selectedItem);
		}

		/*
		 * save items to db methods
		 */
		public void SaveItemsToDb(List<DBItem> items)
		{
			DBManager.Instance.UpdateItems (items);
		}
		/*
		 * get items from db
		 */
		private void GetItemsFromDb(bool sendRequest)
		{
			// set obsver to know if updated

			List <DBItem> i = DBManager.Instance.QueryAllItems ();
			if (i != null && i.Count > 0) {
				this.DBItems = i.ToArray ();
				if (sendRequest) {
					// start request
					this.feedRequest ();
				}
			} else {
				feedRequest ();
			}
		}

		/*
		 * remove item from db
		 */

		private void RemoveItemFromDb(int row)
		{
			DBItem selectedItem = DBItems [row];
			DBManager.Instance.RemoveItem (selectedItem);
			GetItemsFromDb (false);
			
		}

		private MvxCommand<int> m_RemoveCommand;
		public ICommand RemoveCommand
		{
			get { 
				return m_RemoveCommand ?? (m_RemoveCommand = new MvxCommand<int>(i=>RemoveItemFromDb(i))); 
			}

		}

		// initialize db manager
		private MvxCommand<string> m_InitializeDbManagerCommand;
		public ICommand InitDbManagerCommand
		{
			get {
				return m_InitializeDbManagerCommand ?? (m_InitializeDbManagerCommand = new MvxCommand<string>(i=>initDbManager(i)));
			}
		}
		private void initDbManager(string dbPath) {
			DBManager.InitializeWithPath (dbPath);
			// set delegate to listen of changes
			DBManager.Instance .OnContentUpdated += () => {
				GetItemsFromDb(false);
			};
		}

		/*
		 * method to get items (implemented for View)
		 */
		private MvxCommand<string> getItemsCommand;
		public ICommand GetItemsCommand
		{
			get {
				return getItemsCommand ?? (getItemsCommand = new MvxCommand<string>(s=>getItems(s)));
			}
		}
		private void getItems(string rssUrl) {
			_rssUrl = rssUrl;
			GetItemsFromDb (true);
		}
	}
}

