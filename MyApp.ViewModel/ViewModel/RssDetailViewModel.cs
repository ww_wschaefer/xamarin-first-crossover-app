﻿using System;
using MvvmCross.Core.ViewModels;
using System.Net;
using System.IO;
using System.Text;
using MyApp.DB;
using MyApp.Manager;
using System.Windows.Input;

namespace MyApp.Core
{
	public class RssDetailViewModel : MvxViewModel
	{
		private DBItem _item;
		public DBItem dbItem
		{
			get { return _item; }
			set {
				_item = value; 
				RaisePropertyChanged (() => dbItem);
			}
		}

		public string Title { get; set; }

		public void Init(DBItem item)
		{
			// use the parameters here
			dbItem = item;
		}

		public RssDetailViewModel()
		{

		}
		public RssDetailViewModel(string test)
		{

		}

		private string _responseString;
		public string ResponseString
		{
			get { return _responseString; }
			set {
				_responseString = value;
				RaisePropertyChanged (() => ResponseString);
			}
		}

		// delegate methods (actions)
		public Action<string>OnDownloadSucceeded;
		public Action<string>OnDownloadFailed;

		public void starRequestForItem()
		{
			string link = dbItem.Link;
			if (link != null && link.Length > 0) {
				detailRequest (link);
			}
		}

		public void detailRequest(string url) {

			RequestManager.Instance.requestWithUrl(url, RequestSuccessCallback, RequestErrorCallback);
		}

		// Request manager callbacks

		public void RequestSuccessCallback(string responseString)
		{
			dbItem.htmlContent = responseString;
			// update in db
			DBManager.Instance.UpdateItemContent (dbItem);

			if (OnDownloadSucceeded != null) {
				OnDownloadSucceeded (responseString);
			}
		}
		public void RequestErrorCallback(Exception e)
		{
			System.Diagnostics.Debug.WriteLine (e.ToString());
			if (OnDownloadFailed != null) {
				OnDownloadFailed (e.ToString ());
			}
		}

		private void litening() {
			System.Diagnostics.Debug.WriteLine("i'm listening too");
		}
		private void listenOnDbManager() {
			// set delegate to listen of changes
			DBManager.Instance .OnContentUpdated += litening;
		}
		private MvxCommand m_ListenDbManagerCommand;
		public ICommand ListenDbManagerCommand
		{
			get {
				return m_ListenDbManagerCommand ?? (m_ListenDbManagerCommand = new MvxCommand(listenOnDbManager));
			}
		}


		private void unlistenDbManager() {
			// remove delegate to listen of changes
			DBManager.Instance .OnContentUpdated -= litening;
		}
		private MvxCommand m_UnlistenDbManagerCommand;
		public ICommand UnlistenDbManagerCommand
		{
			get {
				return m_UnlistenDbManagerCommand ?? (m_UnlistenDbManagerCommand = new MvxCommand(unlistenDbManager));
			}
		}
	}
}

