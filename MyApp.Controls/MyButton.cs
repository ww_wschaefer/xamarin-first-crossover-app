﻿using System;
using UIKit;

namespace MyApp.Controls
{
	public class MyButton : UIButton
	{
		public MyButton () : base()
		{
			Initialize ();
		}

		void Initialize()
		{
			// load image from bundle
			UIImage image = UIImage.FromFile("Images/test_icon.png");

			this.SetImage (image, UIControlState.Normal);
		}
	}
}

