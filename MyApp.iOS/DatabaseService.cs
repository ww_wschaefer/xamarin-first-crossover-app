﻿using System;
using System.IO;
using MyApp.Core;

namespace MyApp.iOS
{
	public class DatabaseService
	{
		public DatabaseService ()
		{
			
		}

		public static string GetDatabasePath(string dbName)
		{
			string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), String.Format("{0}.db", dbName));
			return path;
		}
	}
}

