﻿
using System;

using Foundation;
using UIKit;
using MvvmCross.iOS.Views;
using MyApp.Core;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Plugins.Messenger;
using MyApp.ViewModel;
using Chance.MvvmCross.Plugins.UserInteraction;
using ObjCRuntime;
using MyApp.Controls;

namespace MyApp.iOS
{
	public partial class FirstView : MvxViewController
	{
		public RssChannel[] TableData {get; set;}
		public CustomActivityView ActivityView { get; set; }

		private UIRefreshControl RefreshControl;

		private string _rssUrl = null;
		private NSObject _observer;

		public FirstView () : base ("FirstView", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// set observer for settings changes
			_observer = NSNotificationCenter.DefaultCenter.AddObserver ((NSString)"NSUserDefaultsDidChangeNotification", DefaultsChanged);
			DefaultsChanged (null);

			var source = new TableViewSource (tableView);
			source.TableDelegate = this;
			
			var set = this.CreateBindingSet<FirstView, FirstViewModel> ();
			set.Bind (source).To (ViewModel => ViewModel.DBItems);
			set.Apply ();

			tableView.Source = source;
			tableView.ReloadData ();

			string dbPath = DatabaseService.GetDatabasePath ("myapp");

			((FirstViewModel)ViewModel).InitDbManagerCommand.Execute (dbPath);

			addRefreshControl ();

			source.ViewModel = ((FirstViewModel)ViewModel);

			((FirstViewModel)ViewModel).GetItemsCommand.Execute (_rssUrl);

			// load image from bundle
//			UIImage image = UIImage.FromBundle("Images/test_icon.png");
// 			this.imageView.Image = image;

			MyButton button = new MyButton ();

			View.Add (button);

			button.TranslatesAutoresizingMaskIntoConstraints = false;

			View.AddConstraint (NSLayoutConstraint.Create (View, NSLayoutAttribute.Right, NSLayoutRelation.Equal, button, NSLayoutAttribute.Right, 1, 10));
			View.AddConstraint (NSLayoutConstraint.Create (button, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View, NSLayoutAttribute.Top, 1, 74));
			View.AddConstraint (NSLayoutConstraint.Create (button, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 64)); 
			View.AddConstraint (NSLayoutConstraint.Create (button, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 64)); 
		}

		void DefaultsChanged(NSNotification obj)
		{
			_rssUrl = NSUserDefaults.StandardUserDefaults.StringForKey("settings_rss_url");
			//_rssUrl = Settings.RssUrl.ToString();
		}


		public void DidSelectRowWithIndexPath(NSIndexPath indexPath) 
		{
			((FirstViewModel) ViewModel).DidSelectRow(indexPath.Row);
		}

		// refresh control

		public void addRefreshControl() {

			if (UIDevice.CurrentDevice.CheckSystemVersion (6,0)) {
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.ValueChanged += HandleValueChanged;
				((FirstViewModel)ViewModel).OnDownloadSucceeded += (jsonString) => {
					Console.WriteLine ("OnDownloadSucceeded");
					InvokeOnMainThread (() => {
						RefreshControl.EndRefreshing ();
						CustomActivityView.Instance.Hide ();
					});
				};
				((FirstViewModel)ViewModel).OnDownloadFailed += (err) => {
					Console.WriteLine ("OnDownloadFailed");
					InvokeOnMainThread (() => {
						RefreshControl.EndRefreshing ();
						CustomActivityView.Instance.Hide ();
					});
				};
				tableView.Add(RefreshControl);
			} 
		}

		// UIRefreshControl iOS6
		void HandleValueChanged (object sender, EventArgs e)
		{
			CustomActivityView.Instance.Show (View, UIScreen.MainScreen.Bounds);
			((FirstViewModel)ViewModel).feedRequest ();
		}
	}
		
	// table view source

	public class TableViewSource : MvxTableViewSource
	{
		public object TableDelegate = null;
		public FirstViewModel ViewModel;

		public TableViewSource(UITableView tableView)
			: base(tableView)
		{
			tableView.RegisterNibForCellReuse(UINib.FromName("RssTableViewCell", NSBundle.MainBundle), RssTableViewCell.CellIdentifier);
		}

		public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return RssTableViewCell.CellHeight();
		}

		protected override UITableViewCell GetOrCreateCellFor (UITableView tableView, NSIndexPath indexPath, object item)
		{
			var cellName = RssTableViewCell.CellIdentifier;				
			return (UITableViewCell)tableView.DequeueReusableCell(cellName, indexPath);
		}
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (TableDelegate != null && typeof(FirstView) == TableDelegate.GetType() ) {
				FirstView v = (FirstView)TableDelegate;
				v.DidSelectRowWithIndexPath (indexPath);
			}
		}

		/*
		 * cell editing mode
		 */
		public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
		{
			return true;
		}
			
		public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
		{
			switch (editingStyle)
			{
			case UITableViewCellEditingStyle.Delete:
				if (ViewModel != null) {
					ViewModel.RemoveCommand.Execute (indexPath.Row);
				}
				break;
			case UITableViewCellEditingStyle.None:
				break;
			}
		}

		public override UITableViewCellEditingStyle EditingStyleForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return UITableViewCellEditingStyle.Delete;
		}

		public override bool CanMoveRow(UITableView tableView, NSIndexPath indexPath)
		{
			return false;
		}
	}
}

