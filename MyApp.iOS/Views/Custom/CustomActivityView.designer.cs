// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace MyApp.iOS
{
	partial class CustomActivityView
	{
		[Outlet]
		UIKit.UIActivityIndicatorView activityView { get; set; }

		[Outlet]
		UIKit.UIView contentView { get; set; }

		[Outlet]
		UIKit.UILabel descriptionLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (activityView != null) {
				activityView.Dispose ();
				activityView = null;
			}

			if (descriptionLabel != null) {
				descriptionLabel.Dispose ();
				descriptionLabel = null;
			}

			if (contentView != null) {
				contentView.Dispose ();
				contentView = null;
			}
		}
	}
}
