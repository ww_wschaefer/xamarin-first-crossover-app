﻿using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using CoreGraphics;

namespace MyApp.iOS
{
	[Register("CustomActivityView")]
	public partial class CustomActivityView : UIView
	{
		private static CustomActivityView instance = null;
		private static readonly object padlock = new object();

		private UIView _customView;

		public static CustomActivityView Instance
		{
			get
			{ 
				lock (padlock) {
					if (instance == null) {
						instance = new CustomActivityView ();
					}
					return instance;
				}
			}
		}

		private CustomActivityView(IntPtr p) : base(p)
		{
			
		}
		private CustomActivityView ()
		{
			NSArray arr = NSBundle.MainBundle.LoadNib("CustomActivityView", this, null);
			CustomActivityView v = Runtime.GetNSObject(arr.ValueAt(0)) as CustomActivityView;
			AddSubview(v);
			this._customView = v;

			v.UserInteractionEnabled = false;
			this.contentView.Layer.CornerRadius = 5.0f;
		}

		public void ShowInView(UIView v)
		{
			Show (v, v.Frame);
		}

		public void Show(UIView v, CGRect frame)
		{
			v.AddSubview (this);
			this._customView.Frame = frame;
		}
		public void Hide() {
			// check have superview
			if (this.Superview != null) {
				this.RemoveFromSuperview ();
			}
		}
	}
}

