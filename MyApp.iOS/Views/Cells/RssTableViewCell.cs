﻿
using System;

using Foundation;
using UIKit;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platform;
using MyApp.Core;
using MyApp.DB;

namespace MyApp.iOS
{
	public partial class RssTableViewCell : MvxTableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("RssTableViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("RssTableViewCell");
		public static readonly NSString CellIdentifier = new NSString("RssCell");

		private MvxImageViewLoader _imageHelper;

		public static RssTableViewCell Create ()
		{
			return (RssTableViewCell)Nib.Instantiate (null, null) [0];
		}
		public static float CellHeight() {
			return 110;
		}
		public RssTableViewCell() : base()
		{
			Initialize ();
		}
		public RssTableViewCell(IntPtr handle) : base (handle)
		{
			Initialize ();
		}
		private void Initialize()
		{
			_imageHelper = new MvxImageViewLoader(() => thumbImageView);
			this.DelayBind (() => {
				var bindings = this.CreateBindingSet<RssTableViewCell, DBItem>();
				bindings.Bind(_imageHelper).To(rssItem => rssItem.MediaThumbnail);
				bindings.Bind(titleLabel).To(rssItem => rssItem.Title);
				bindings.Bind(descriptionLabel).To(rssItem => rssItem.PubDate);
				bindings.Apply();
			});
		}

		protected override void Dispose (bool disposing)
		{
			if (disposing) {
				if (_imageHelper != null)
				{
					_imageHelper.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}

