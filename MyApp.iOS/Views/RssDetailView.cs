﻿
using System;

using Foundation;
using UIKit;
using MvvmCross.iOS.Views;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Binding.BindingContext;
using MyApp.Core;
using MyApp.DB;

namespace MyApp.iOS
{
	public partial class RssDetailView : MvxViewController 
	{
		public RssDetailView () : base ("RssDetailView", null)
		{
		}
		private string _responseString;
		public string ResponseString {
			get { return _responseString; }
			set {
				_responseString = value;
				this.webView.LoadHtmlString (_responseString, null);
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			webView.Delegate = new MyWebViewDelegate();

			// todo alwais unlisten if leave view
			((RssDetailViewModel)ViewModel).ListenDbManagerCommand.Execute("");

			DBItem item = ((RssDetailViewModel)ViewModel).dbItem;
			if (item != null && item.htmlContent != null) {
				webView.LoadHtmlString (item.htmlContent, null);
			} else {
				((RssDetailViewModel)ViewModel).OnDownloadSucceeded += (htmlContent) => {
					Console.WriteLine ("OnDownloadSucceeded");
					InvokeOnMainThread (() => {
						webView.LoadHtmlString (htmlContent, null);
						CustomActivityView.Instance.Hide();
					});
				};
				((RssDetailViewModel)ViewModel).OnDownloadFailed += (err) => {
					Console.WriteLine ("OnDownloadFailed");
					CustomActivityView.Instance.Hide();
				};
				CustomActivityView.Instance.Show (View, UIScreen.MainScreen.Bounds);
				((RssDetailViewModel)ViewModel).starRequestForItem ();
			}
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			((RssDetailViewModel)ViewModel).UnlistenDbManagerCommand.Execute("");
		}
	}

	public class MyWebViewDelegate: UIWebViewDelegate
	{   
		private int _requestCount = 0;

		public override bool ShouldStartLoad (UIWebView webView, 
			NSUrlRequest request,
			UIWebViewNavigationType navigationType)
		{
//			System.Console.WriteLine("Starting load");
			return true;
		}

		public override void LoadStarted (UIWebView webview)
		{
//			System.Console.WriteLine("LoadStarted");
//			CustomActivityView.Instance.Show (webview, UIScreen.MainScreen.Bounds);
		}

		public override void LoadingFinished (UIWebView webView) 
		{
//			System.Console.WriteLine("LoadingFinished");
//			CustomActivityView.Instance.Hide();
		}
	}
}

