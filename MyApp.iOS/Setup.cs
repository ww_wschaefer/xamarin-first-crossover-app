﻿using System;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MyApp.Core;

namespace MyApp.iOS
{
	public class Setup : MvxIosSetup
	{
		public Setup(MvxApplicationDelegate app, IMvxIosViewPresenter presenter) : base(app, presenter)
		{
			
		}
		protected override MvvmCross.Core.ViewModels.IMvxApplication CreateApp ()
		{
			return new Core.App ();
		}

		protected override void InitializeIoC ()
		{
			base.InitializeIoC ();
		}
	}
}

