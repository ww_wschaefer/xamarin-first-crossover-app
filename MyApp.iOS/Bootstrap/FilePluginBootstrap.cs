using MvvmCross.Platform.Plugins;

namespace MyApp.iOS.Bootstrap
{
    public class FilePluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.File.PluginLoader, MvvmCross.Plugins.File.iOS.Plugin>
    {
    }
}