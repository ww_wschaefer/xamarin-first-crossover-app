﻿using System;
using System.Net;
using System.IO;
using System.Text;

namespace MyApp.Manager
{
	public class RequestManager
	{
		private static RequestManager instance = null;
		//private static readonly object padlock = new Object();

		private RequestManager ()
		{
		}
		public static RequestManager Instance
		{
			get
			{ 
//				lock (padlock) {
					if (instance == null) {
						instance = new RequestManager ();
					}
					return instance;
				}
//			}
		}

		// async request method

		public void requestWithUrl(string urlString, Action<string> successCallback, Action<Exception> failedCallback)
		{
			WebRequest request = WebRequest.Create(urlString);
			request.BeginGetResponse(new AsyncCallback( (IAsyncResult result) =>
				{
					if (result.IsCompleted) {
						WebResponse response = request.EndGetResponse(result);
						Stream responseStream = response.GetResponseStream();
						StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
						String responseString = reader.ReadToEnd();
						successCallback(responseString);
					}
				}
			), null);
		}
	}
}

