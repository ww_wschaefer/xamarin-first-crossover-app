﻿using System;
using System.Collections.Generic;
using System.Linq;
using Path = System.IO.Path;
using MyApp.DB;
using MyApp.Core;

namespace MyApp.Manager
{
	public class DBManager
	{

		public Action OnContentUpdated;

		private static DBManager instance = null;

		Database _db;

		private DBManager (string dbPath)
		{
			_db = new Database(dbPath);
		}

		public static void InitializeWithPath(string dbPath)
		{
			if (instance == null) {
				instance = new DBManager (dbPath);
			}
		}
		public static DBManager Instance
		{
			get {
				if (instance == null) {
					throw new Exception ("DB Manager is not initialized, call InitializeWithPath(string dbPath) before use.", null);
				}
				return instance;
			}
		}

		public List<DBItem> QueryAllItems ()
		{

			var result = _db.QueryAllItems ().ToList();
			return result;
		}

		public void InsertItem(DBItem Item)
		{
			_db.Insert (Item);
		}
		public void InsertItems(List<DBItem> Items)
		{
			_db.InsertAll (Items, true);
		}

		public void UpdateItems(List<DBItem> Items)
		{
			foreach (DBItem item in Items) {
				// check ist item present in db
				System.Diagnostics.Debug.WriteLine (String.Format ("check item: {0}", item.Title));
				DBItem storedItem = _db.QueryItem(item.Id);
				if (storedItem == null) {
					// insert in db
					_db.Insert (item, typeof(DBItem));
					System.Diagnostics.Debug.WriteLine (String.Format ("{0} inserted!", item.ToString ()));
				} else {
					//System.Diagnostics.Debug.WriteLine (String.Format ("{0} is allredy presented", item.ToString ()));
					storedItem.PubDate = item.PubDate;
					storedItem.Description = item.Description;

					_db.Update(storedItem);
				}
			}
		}

		public void UpdateItemContent(DBItem item)
		{
			DBItem storedItem = _db.QueryItem(item.Id);
			if (storedItem != null) {
				storedItem.htmlContent = item.htmlContent;
				_db.Update (storedItem);
				if (OnContentUpdated != null) {
					OnContentUpdated ();
				}
			}
		}

		public void CleanItems()
		{
			_db.DeleteAll<DBItem>();
		}

		public void RemoveItem(DBItem Item)
		{
			_db.Delete<DBItem>(Item.Id);
		}
	}
}

